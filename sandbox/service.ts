import { DateFormatDirective } from '@geeebe/graphql';
import { IServiceOptions, KoaService } from '@geeebe/service';
import { ApolloServer, makeExecutableSchema } from 'apollo-server-koa';
import fs = require('fs');
import Router = require('koa-router');
import { createResolvers } from './resolvers';

const typeDefs = [
  fs.readFileSync(require.resolve('./service.graphql')).toString(),
];

export interface IDataServiceOptions extends IServiceOptions {
  development: boolean;
}

/**
 * GraphQL Data Service
 */
export class GraphQlService extends KoaService<IDataServiceOptions> {
  constructor(options: IDataServiceOptions) {
    super(options);
  }

  public mountApi(_router: Router): void {
    const resolvers = createResolvers();
    const schema = makeExecutableSchema({
      resolvers,
      schemaDirectives: {
        date: DateFormatDirective,
      },
      typeDefs,
    });
    new ApolloServer({ schema }).applyMiddleware({ app: this, path: '/graphql' });
  }
}
