import { IFragmentMap, Selection, simplifyInfo } from '@geeebe/graphql';
import { Document, Model, Schema } from 'mongoose';

export type FindArgs = [any, any | null, any | null];

export type ConditionModifier = (parent: any, args: any, context: any) => any;

export function findMany<T extends Document>(model: Model<T>, ...conditionModifiers: ConditionModifier[]) {
  return async (obj: any, args: any, context: any, info: any) => {
    return await model.find(
      ...parse(model.schema, obj, args, context, info, ...conditionModifiers),
    );
  };
}

export function findOne<T extends Document>(model: Model<T>, ...conditionModifiers: ConditionModifier[]) {
  return async (obj: any, args: any, context: any, info: any) => {
    return await model.findOne(
      ...parse(model.schema, obj, args, context, info, ...conditionModifiers),
    );
  };
}

function combineAnd($and: any[]) {
  const filtered = $and.filter((item) => item && Object.keys(item).length);
  if (filtered.length === 0) return {};
  if (filtered.length === 1) return filtered[0];
  return { $and: filtered };
}

export function parse(schema: Schema, obj: any, args: any, context: any, info: any, ...conditionModifiers: ConditionModifier[]): FindArgs {
  const options: any = {};
  const $and = [
    ...conditionModifiers.map((modifier) => modifier(obj, args, context)),
  ];
  if (args) {
    if ('_id' in args) {
      $and.push({ _id: args._id });
    }
    Object.assign(options, args.paging);
  }
  const condition = combineAnd($and);

  return [
    condition,
    infoToProjection(info, schema),
    options,
  ];
}

export function infoToProjection(infoAst: any, schema: Schema) {
  const info = simplifyInfo(infoAst);
  const projection: { [key: string]: true } = {};

  info.fieldNodes.forEach((node) => {
    Object.assign(
      projection,
      selectionSetToProjection(node.selections, schema.obj, info.fragments),
    );
  });
  projection.whenUpdated = true;
  return projection;
}

export function selectionSetToProjection(selections: Selection[], schemaObj: any, fragments: IFragmentMap) {
  const projection: { [key: string]: true } = {};

  selections.forEach((selection) => {
    if (!selection) return;
    switch (selection.kind) {
      case 'Field': {
        const field = selection.name;
        if (!schemaObj[field]) return;
        if (selection.selections.length) {
          // this is a child object or array
          const childProjection = selectionSetToProjection(selection.selections, schemaObj[field], fragments);
          for (const childField of Object.keys(childProjection)) {
            projection[`${field}.${childField}`] = true;
          }
        } else {
          // normal field
          projection[field] = true;
        }
        break;
      }
      case 'FragmentSpread': {
        const fragment = fragments[selection.name];
        if (!fragment || !fragment.selections.length) return;
        Object.assign(
          projection,
          selectionSetToProjection(fragment.selections, schemaObj, fragments),
        );
        break;
      }
    }
  });
  return projection;
}
