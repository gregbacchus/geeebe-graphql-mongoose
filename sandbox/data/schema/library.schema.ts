import { Document, Schema } from 'mongoose';
import * as uuid from 'uuid';

export const schema = new Schema({
  _id: { type: String, required: true, default: uuid.v4 },
  name: { type: String, required: true },
  operatorIds: [{ type: String, required: true }],
});

export interface ILibrary extends Document {
  _id: string;
  name: string;
  operatorIds: string[];
}
