import { findMany, findOne } from '../../src/resolver';
import { Data } from '../data';

export default () => {
  return {
    test1: async () => {
      return await Data.user.findOne({});
    },
    user: findOne(Data.user),
    users: findMany(Data.user),
  };
};
