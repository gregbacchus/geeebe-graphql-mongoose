import { DateFormatDirective } from '@geeebe/graphql';
import { ApolloServer, makeExecutableSchema } from 'apollo-server-koa';
import fs = require('fs');
import Koa = require('koa');
import bodyParser = require('koa-bodyparser');
import * as mongoose from 'mongoose';
import { Server } from 'net';
import request = require('supertest');
import { createResolvers } from '../sandbox/resolvers';

const typeDefs = [
  fs.readFileSync(require.resolve('../sandbox/service.graphql')).toString(),
];

const resolvers = createResolvers();
const schema = makeExecutableSchema({
  resolvers,
  schemaDirectives: {
    date: DateFormatDirective,
  },
  typeDefs,
});
const graph = new ApolloServer({ schema });

describe('simplify', () => {
  const app = new Koa();
  app.use(bodyParser());
  graph.applyMiddleware({ app, path: '/' });

  let server: Server;

  beforeAll(() => {
    server = app.listen();
  });

  afterAll(async () => {
    await mongoose.connection.close(true);
    await mongoose.disconnect();
    server.close();
  });

  it('must do stuff', async () => {
    const response: any = await request(server)
      .post('/')
      .send({
        query: '{user {_id}}',
      });
    expect(response).toBeDefined();
    expect(response.status).toBe(200);
    expect(response.body.data).toBeDefined();
    console.log(response.body);
  });
});
