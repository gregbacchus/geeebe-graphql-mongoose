import { logger } from '@geeebe/logging';
import { env } from './env';
import { GraphQlService } from './service';

const log = logger.child({ module: '@geeebe/graphql:index' });

// create service with options
const service = new GraphQlService({
  development: env.DEV,
  port: env.PORT,
});

// fire it up!
service.start();
log(`listening on ${env.PORT}...`);
