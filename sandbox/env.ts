import envalid = require('envalid');
import { bool, port, url } from 'envalid';

export const env = envalid.cleanEnv(process.env, {
  DEV: bool({ default: false }),
  MONGO_URL: url({ default: 'mongodb://127.0.0.1:27017', desc: 'MongoDB connection string URL' }),
  PORT: port({ default: 80 }),
});
