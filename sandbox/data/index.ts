import { Mongo } from '@geeebe/data';
import * as mongoose from 'mongoose';
import { env } from '../env';
import * as Library from './schema/library.schema';
import * as User from './schema/user.schema';

const mongo = new Mongo(mongoose, env.MONGO_URL);

export namespace Data {
  export const library = mongo.createModel<Library.ILibrary>('Library', Library.schema, true);
  export const user = mongo.createModel<User.IUser>('User', User.schema, true);
}
