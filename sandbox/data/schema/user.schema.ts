import { Document, Schema } from 'mongoose';
import * as uuid from 'uuid';

export const schema = new Schema({
  _id: { type: String, required: true, default: uuid.v4 },
  address: {
    _id: false,
    address: { type: String },
    city: { type: String },
    postCode: { type: String },
  },
  dateOfBirth: { type: Date },
  email: { type: String },
  name: { type: String, required: true },
});

export interface IUser extends Document {
  _id: string;
  address: {
    address: string;
    city: string;
    postCode: string;
  };
  whenUpdated: Date;
  email: string;
  name: string;
}
