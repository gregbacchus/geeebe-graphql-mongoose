import { IResolvers } from 'graphql-tools';
import query from './query';
import user from './user';

/**
 * The resolvers map the GraphQL query to database queries.
 */
export function createResolvers(): IResolvers {
  return {
    Query: query(),
    User: user(),
  };
}
