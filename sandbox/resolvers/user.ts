import { findMany } from '../../src/resolver';
import { Data } from '../data';
import { IUser } from '../data/schema/user.schema';

export default () => {
  return {
    address: async (obj: any, _args: any, _context: any, _info: any) => {
      return obj.address;
    },
    libraries: findMany(Data.library, (parent: IUser) => {
      console.log(parent);
      return {
        operatorIds: parent._id,
      };
    }),
  };
};
